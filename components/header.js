import Link from "next/link";

export default function Header() {
  return (
    <h2 className="text-2xl md:text-4xl font-bold tracking-tight md:tracking-tighter leading-tight mb-20 mt-8">
      <Link href="/">
        <a className="hover:underline">
          {" "}
          <img
            src="/rose.svg"
            height="50px"
            width="50px"
            style={{ position: "relative", top: "5px", left: "5px" }}
          ></img>
          Ridván
        </a>
      </Link>
      .
    </h2>
  );
}
