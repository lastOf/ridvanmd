import { CMS_NAME } from '../lib/constants'

export default function Intro() {
  return (
    <>
    <a href="https://ridvan.org"><img src="/rose.svg" height="75px" width="75px" style={{position:"fixed", top: "50px", left: "50px"}}></img></a>
    <section className="flex-col md:flex-row flex items-center md:justify-between mt-16 mb-16 md:mb-12">
      
      <h1 className="text-6xl md:text-8xl font-bold tracking-tighter leading-tight md:pr-8">
       Ridván
      </h1>
      <h4 className="text-center md:text-left text-lg mt-5 md:pl-8">
        Software agency and consulting from{' '}
        <a
          href="https://portfoliosaurus.now.sh/"
          target="_blank"
          className="underline hover:text-success duration-200 transition-colors"
          >
          Quddús
        </a>
        .
      </h4>
    </section>
          </>
  )
}
