---
title: 'Open for business'
excerpt: 'Based in Waterloo South Carolina offering modern websites, mobile applications, deploying, configuring and managing open source software.'
coverImage: '/assets/blog/dynamic-routing/song.jpg'
date: '2020-09-25'
author:
  name: Quddús George
  picture: ''
ogImage:
  url: '/assets/blog/dynamic-routing/song.jpg'
---

Need help navigating todays software landscape?

Ridván is open for business!

Based in Waterloo South Carolina offering modern websites, mobile applications, deploying, configuring and managing open source software.

Reach out at **lastof[a]pm.me** for a consultation.
